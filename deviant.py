#import deviantart library
import subprocess
import deviantart

#create an API object with your client credentials
da = deviantart.Api("id", "pw")

#fetch daily deviations

offset = 0
while(True):
    deviations = da.get_collection("F6FCF336-AA5C-9D5E-8CC2-F9F3233BD141", "qube-core", offset)
    offset += 10
    #dailydeviations = da.get_collections("qube-core")

    #loop over daily deviations
    for deviation in deviations["results"]:
        dev = da.get_deviation(deviation)
        
        if (dev.is_downloadable):
            print("Is downloadable")
            dldev = da.download_deviation(dev.deviationid)
            print("" + str(dldev["width"]) + " x " + str(dldev["height"]))
            print(dldev["src"])
            w = dldev["width"]
            h = dldev["height"]
            if (w >= 1280 and h >= 720):
                subprocess.call(["wget", dldev["src"], "-O", "" + dev.deviationid + ".png"])
            

        print dev.deviationid
