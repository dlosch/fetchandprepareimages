for file in *.png;
do
	geometry=$(identify -verbose $file | grep Geometry | cut -d' ' -f4)
	imgw=$(echo $geometry | cut -d'x' -f1)
	imgh=$(echo $geometry | cut -d'x' -f2 | cut -d'+' -f1)
	echo "Img res: $imgw x $imgh"
	if [ $imgh -gt $imgw ] 
	then
		echo "Img $file is in portrait"
		mogrify -rotate "90" $file
		tmp=$imgw
		imgw=$imgh
		imgh=$tmp
	fi
	filebase=$(echo $file | cut -d'.' -f1)
	echo $filebase
	if [ $imgw -gt 1920 ]
	then
		echo "Resizing"
		convert $file -resize 1920x1080 $filebase.jpg
		rm $file
	else
		mv $file $filebase.jpg
	fi



done
